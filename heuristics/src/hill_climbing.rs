use std::collections::BTreeSet;
use crate::{Heuristic, Task};
use crate::constructive::{ConstructiveHeuristicLessLateLessWeight, ConstructiveHeuristicLessLateMostWeight, ConstructiveHeuristicMostLateLessWeight, ConstructiveHeuristicMostLateMostWeight};
use crate::heuristic::{BorrowedScheduler, Scheduler};

pub struct HillClimbingNaiveRandom;
pub struct HillClimbingNaiveLL;
pub struct HillClimbingNaiveLM;
pub struct HillClimbingNaiveML;
pub struct HillClimbingNaiveMM;
// ---
pub struct HillClimbingConstantUpgradeRandom;
pub struct HillClimbingConstantUpgradeLL;
pub struct HillClimbingConstantUpgradeLM;
pub struct HillClimbingConstantUpgradeML;
pub struct HillClimbingConstantUpgradeMM;
// ---
pub struct VndRandom;
pub struct VndLL;
pub struct VndLM;
pub struct VndML;
pub struct VndMM;

// --- Naive ---

pub fn climbing_naively_unconverted(mut best_neighbour:BorrowedScheduler) -> BorrowedScheduler {
    let mut best_quality = best_neighbour.verify();
    let cpy = best_neighbour.clone();
    let items = Box::new(cpy.get_refs());

    let mut config_seen:BTreeSet<Box<Vec<&Task>>> = BTreeSet::new();
    config_seen.insert(items.clone());

    let no_items = items.len();
    for i in 0..no_items {
        for j in (i+1)..no_items {
            let mut nghb = items.clone(); // 100
            nghb[i] = items[j];
            nghb[j] = items[i];

            if !config_seen.contains(&nghb) {
                let ng = nghb.clone(); // 100
                let sch = BorrowedScheduler::create_scheduler(&ng);
                let q = sch.verify();
                if q < best_quality {
                    best_quality = q;
                    best_neighbour = sch;
                }
                config_seen.insert(nghb);
            }
        }
    }

    best_neighbour
}

fn climbing_naively(scheduler:Scheduler) -> Scheduler{
    let scheduler = BorrowedScheduler::from(scheduler);
    climbing_naively_unconverted(scheduler).to_owned()
}


// -- Constant upgrade ---

fn climbing_constant_upgrade(scheduler:Scheduler) -> Scheduler{
    let mut best_neighbour = BorrowedScheduler::from(scheduler);
    let mut best_quality = best_neighbour.verify();
    let cpy = best_neighbour.clone();
    let items = Box::new(cpy.get_refs());
    let no_items = items.len();
    let mut config_seen:BTreeSet<Box<Vec<&Task>>> = BTreeSet::new();
    let mut config_to_test:Vec<Box<Vec<&Task>>> = Vec::new();

    let neighbour = items.clone();
    config_to_test.push(items);
    config_seen.insert(neighbour);

    while let Some(items) = config_to_test.pop() {
        for i in 0..no_items {
            for j in (i+1)..no_items {
                let mut nghb = items.clone(); // 100
                nghb[i] = items[j];
                nghb[j] = items[i];

                if !config_seen.contains(&nghb) {
                    let ng = nghb.clone(); // 100
                    let sch = BorrowedScheduler::create_scheduler(&ng);
                    let q = sch.verify();
                    if q < best_quality {
                        best_quality = q;
                        best_neighbour = sch;
                        config_to_test.push(nghb.clone()); // 100
                    }
                    config_seen.insert(nghb);
                }
            }
        }
    }

    best_neighbour.clone().to_owned()
}


// --- VND ---
// fn compute_vnd_style(scheduler:Scheduler) -> Scheduler{
//     let items:Vec<&Task> = scheduler.tasks
//         .iter()
//         .map(|(t, _)|{t})
//         .collect();
//
//     let fst = BorrowedScheduler::create_scheduler(&items);
//     let mut best_neighbour = fst.0;
//     let items = fst.1;
//     let mut best_quality = best_neighbour.verify();
//
//
//     let max_items:usize = items.len();
//     let max_iter:usize = max_items / 4;
//
//     let mut config_seen:BTreeSet<Vec<&Task>> = BTreeSet::new();
//     let mut config_to_test:Vec<Vec<&Task>> = Vec::new();
//     config_seen.insert(items.clone());
//     config_to_test.push(items);
//
//     for iter in 0..max_iter {
//         while let Some(items) = config_to_test.pop() {
//             let l = config_to_test.len();
//             println!("still {l} items to do..");
//             // n * (n-1)/2 iter
//             for i in 0..max_items {
//                 for j in (i+1)..(max_items-iter) {
//                     let mut other = items.clone(); // 100
//                     other[i] = items[j+iter];
//                     other[j+iter] = items[i];
//                     if !config_seen.contains(&other) {
//                         let (sch, o) = BorrowedScheduler::create_scheduler(Box::new(other)); // 100
//                         let q = sch.verify();
//                         if q < best_quality {
//                             best_quality = q;
//                             best_neighbour = sch;
//                             config_to_test.push(other);
//                         }
//                         config_seen.insert(o);
//                     }
//                 }
//             }
//         }
//     }
//
//     best_neighbour.to_owned()
// }



// --- Implementations ---
// === Naive ===

impl Heuristic for HillClimbingNaiveRandom {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler{
        let scheduler = crate::random::Random.solve_instance(tasks);
        climbing_naively(scheduler)
    }
}
impl Heuristic for HillClimbingNaiveLL {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler{
        let scheduler = ConstructiveHeuristicLessLateLessWeight.solve_instance(tasks);
        climbing_naively(scheduler)
    }
}
impl Heuristic for HillClimbingNaiveLM {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler{
        let scheduler = ConstructiveHeuristicLessLateMostWeight.solve_instance(tasks);
        climbing_naively(scheduler)
    }
}
impl Heuristic for HillClimbingNaiveML {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler{
        let scheduler = ConstructiveHeuristicMostLateLessWeight.solve_instance(tasks);
        climbing_naively(scheduler)
    }
}
impl Heuristic for HillClimbingNaiveMM {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler{
        let scheduler = ConstructiveHeuristicMostLateMostWeight.solve_instance(tasks);
        climbing_naively(scheduler)
    }
}

// === Constant ===
impl Heuristic for HillClimbingConstantUpgradeRandom {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler{
        let scheduler = crate::random::Random.solve_instance(tasks);
        climbing_constant_upgrade(scheduler)
    }
}
impl Heuristic for HillClimbingConstantUpgradeLL {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler{
        let scheduler = ConstructiveHeuristicLessLateLessWeight.solve_instance(tasks);
        climbing_constant_upgrade(scheduler)
    }
}
impl Heuristic for HillClimbingConstantUpgradeLM {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler{
        let scheduler = ConstructiveHeuristicLessLateMostWeight.solve_instance(tasks);
        climbing_constant_upgrade(scheduler)
    }
}
impl Heuristic for HillClimbingConstantUpgradeML {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler{
        let scheduler = ConstructiveHeuristicMostLateLessWeight.solve_instance(tasks);
        climbing_constant_upgrade(scheduler)
    }
}
impl Heuristic for HillClimbingConstantUpgradeMM {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler{
        let scheduler = ConstructiveHeuristicMostLateMostWeight.solve_instance(tasks);
        climbing_constant_upgrade(scheduler)
    }
}
