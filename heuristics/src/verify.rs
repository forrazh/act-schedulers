use crate::heuristic::Scheduler;
use std::fs::File;
use std::io::Write;
use crate::Task;

pub fn verify(scheduler: &Scheduler) -> u128 {
    let lateness = scheduler
        .tasks
        .iter()
        .map(|(t, c)| t.weight as u128 * c.lateness)
        .reduce(|a, b| a + b);

    lateness.unwrap()
}

pub fn show_scheduler(scheduler: &Scheduler) {
    for (t, _) in &scheduler.tasks {
        let p = t.period;
        let w = t.weight;
        let d = t.due_to;

        println!("{p} {w} {d}");
    }
}

pub fn show_scheduler_in_file(mut file: File, scheduler: &Scheduler) {
    println!("=========");
    let mut tasks: Vec<_> = scheduler
        .tasks
        .iter()
        .collect();
    tasks.sort_by(|(_, c), (_, c2)| c.completed_at.cmp(&c2.completed_at));
    for (t, _) in tasks {
        let p = t.id;
        writeln!(&mut file, "{p}").unwrap();
    }
}
