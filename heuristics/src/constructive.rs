use crate::heuristic::{CompletionData, Heuristic, Scheduler, Task};
use crate::take_as_is;

pub struct ConstructiveHeuristicMostWeight;
pub struct ConstructiveHeuristicLessWeight;
pub struct ConstructiveHeuristicLessLate;
pub struct ConstructiveHeuristicMostLate;

pub struct ConstructiveHeuristicMostLateMostWeight;
pub struct ConstructiveHeuristicMostLateLessWeight;
pub struct ConstructiveHeuristicLessLateMostWeight;
pub struct ConstructiveHeuristicLessLateLessWeight;

impl Heuristic for ConstructiveHeuristicMostWeight {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let mut t: Vec<Task> = tasks.into_iter().collect();
        t.sort_by(|t1, t2| t1.weight.partial_cmp(&t2.weight).unwrap());
        t.reverse();
        let mut scheduler:Scheduler = Scheduler::new();
        let mut curr_compl:u128 = 0;

        for task in t {
            curr_compl += task.period ;

            let late = if curr_compl > task.due_to { curr_compl - task.due_to } else { 0 };
            let c = CompletionData::new(task.id, curr_compl, late);

            scheduler.tasks.insert(task, c);
        }

        scheduler
    }
}

impl Heuristic for ConstructiveHeuristicLessWeight {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let mut t: Vec<Task> = tasks.into_iter().collect();
        t.sort_by(|t1, t2| t1.weight.partial_cmp(&t2.weight).unwrap());
        let mut scheduler: Scheduler = Scheduler::new();
        let mut curr_compl: u128 = 0;

        for task in t {
            curr_compl += task.period;

            let late = if curr_compl > task.due_to {
                curr_compl - task.due_to
            } else {
                0
            };
            let c = CompletionData::new(task.id, curr_compl, late);

            scheduler.tasks.insert(task, c);
        }

        scheduler
    }
}

impl Heuristic for ConstructiveHeuristicLessLate {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let mut scheduler: Scheduler = Scheduler::new();
        let mut curr_compl: u128 = 0;
        let mut ids: Vec<usize> = (0..tasks.len()).collect();

        while !ids.is_empty() {
            let mut b_i = 0;
            let mut min_late = i32::max_value();
            for (idx, val) in (&ids).into_iter().enumerate() {
                let t = tasks[*val];
                let lateness = t.due_to as i32 - (curr_compl + t.period) as i32;
                if min_late > lateness {
                    min_late = lateness;
                    b_i = idx;
                }
            }
            let x = ids.remove(b_i);
            let t = tasks[x];
            curr_compl += t.period;
            let late = if curr_compl > t.due_to {
                curr_compl - t.due_to
            } else {
                0
            };
            let c = CompletionData::new(t.id, curr_compl, late);

            scheduler.tasks.insert(t, c);
        }

        scheduler
    }
}

impl Heuristic for ConstructiveHeuristicMostLate {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let mut scheduler: Scheduler = Scheduler::new();
        let mut curr_compl: u128 = 0;
        let mut ids: Vec<usize> = (0..tasks.len()).collect();

        while !ids.is_empty() {
            let mut b_i = 0;
            let mut max_late = i32::min_value();
            for (idx, val) in (&ids).into_iter().enumerate() {
                let t = tasks[*val];
                let lateness = t.due_to as i32 - (curr_compl + t.period) as i32;
                if max_late < lateness {
                    max_late = lateness;
                    b_i = idx;
                }
            }
            let x = ids.remove(b_i);
            let t = tasks[x];
            curr_compl += t.period;
            let late = if curr_compl > t.due_to {
                curr_compl - t.due_to
            } else {
                0
            };
            let c = CompletionData::new(t.id, curr_compl, late);
            scheduler.tasks.insert(t, c);
        }

        scheduler
    }
}

impl Heuristic for ConstructiveHeuristicLessLateMostWeight {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let mut scheduler: Scheduler = Scheduler::new();
        let mut curr_compl: u128 = 0;

        let max = (&tasks).into_iter().map(|t| t.weight).max().unwrap() as u128;

        for i in (0..=max).rev() {
            let current_tasks: Vec<Task> = (&tasks).into_iter().partition(|t| t.weight == i).0;
            let mut ids: Vec<usize> = (0..current_tasks.len()).into_iter().collect();

            while !ids.is_empty() {
                let mut b_i = 0;
                let mut max_late = i32::min_value();
                for (idx, val) in (&ids).into_iter().enumerate() {
                    let t = current_tasks[*val];
                    let lateness = t.due_to as i32 - (curr_compl + t.period) as i32;
                    if max_late < lateness {
                        max_late = lateness;
                        b_i = idx;
                    }
                }
                let x = ids.remove(b_i);
                let t = current_tasks[x];

                curr_compl += t.period;
                let late = if curr_compl > t.due_to {
                    curr_compl - t.due_to
                } else {
                    0
                };
                let c = CompletionData::new(t.id, curr_compl, late);
                scheduler.tasks.insert(t, c);
            }
        }

        scheduler
    }
}

impl Heuristic for ConstructiveHeuristicLessLateLessWeight {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let mut scheduler: Scheduler = Scheduler::new();
        let mut curr_compl: u128 = 0;

        let max = (&tasks).into_iter().map(|t| t.weight).max().unwrap() as u128;

        //        let mut tasks_splitted = vec![Vec::new();max];

        for i in 0..=max {
            let current_tasks: Vec<Task> = (&tasks).into_iter().partition(|t| t.weight == i).0;
            let mut ids: Vec<usize> = (0..current_tasks.len()).into_iter().collect();
            while !ids.is_empty() {
                let mut b_i = 0;
                let mut max_late = i32::min_value();
                for (idx, val) in (&ids).into_iter().enumerate() {
                    let t = current_tasks[*val];
                    let lateness = t.due_to as i32 - (curr_compl + t.period) as i32;
                    if max_late < lateness {
                        max_late = lateness;
                        b_i = idx;
                    }
                }
                let x = ids.remove(b_i);
                let t = current_tasks[x];
                curr_compl += t.period;
                let late = if curr_compl > t.due_to {
                    curr_compl - t.due_to
                } else {
                    0
                };
                let c = CompletionData::new(t.id, curr_compl, late);
                scheduler.tasks.insert(t, c);
            }
        }

        scheduler
    }
}

impl Heuristic for ConstructiveHeuristicMostLateMostWeight {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let mut scheduler: Scheduler = Scheduler::new();
        let mut curr_compl: u128 = 0;

        let max = (&tasks).into_iter().map(|t| t.weight).max().unwrap() as u128;

        for i in (0..=max).rev() {
            let current_tasks: Vec<Task> = (&tasks).into_iter().partition(|t| t.weight == i).0;
            let mut ids: Vec<usize> = (0..current_tasks.len()).into_iter().collect();
            while !ids.is_empty() {
                let mut b_i = 0;
                let mut min_late = i32::max_value();
                for (idx, val) in (&ids).into_iter().enumerate() {
                    let t = current_tasks[*val];
                    let lateness = t.due_to as i32 - (curr_compl + t.period) as i32;
                    if min_late > lateness {
                        min_late = lateness;
                        b_i = idx;
                    }
                }
                let x = ids.remove(b_i);
                let t = current_tasks[x];
                curr_compl += t.period;
                let late = if curr_compl > t.due_to {
                    curr_compl - t.due_to
                } else {
                    0
                };
                let c = CompletionData::new(t.id, curr_compl, late);
                scheduler.tasks.insert(t, c);
            }
        }

        scheduler
    }
}

impl Heuristic for ConstructiveHeuristicMostLateLessWeight {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let mut scheduler: Scheduler = Scheduler::new();
        let mut curr_compl: u128 = 0;

        let max = (&tasks).into_iter().map(|t| t.weight).max().unwrap() as u128;

        //        let mut tasks_splitted = vec![Vec::new();max];

        for i in 0..=max {
            let current_tasks: Vec<Task> = (&tasks).into_iter().partition(|t| t.weight == i).0;
            let mut ids: Vec<usize> = (0..current_tasks.len()).into_iter().collect();
            while !ids.is_empty() {
                let mut b_i = 0;
                let mut min_late = i32::max_value();
                for (idx, val) in (&ids).into_iter().enumerate() {
                    let t = current_tasks[*val];
                    let lateness = t.due_to as i32 - (curr_compl + t.period) as i32;
                    if min_late > lateness {
                        min_late = lateness;
                        b_i = idx;
                    }
                }
                let x = ids.remove(b_i);
                let t = current_tasks[x];
                curr_compl += t.period;
                let late = if curr_compl > t.due_to {
                    curr_compl - t.due_to
                } else {
                    0
                };
                let c = CompletionData::new(t.id, curr_compl, late);
                scheduler.tasks.insert(t, c);
            }
        }

        scheduler
    }
}
