use crate::heuristic::{Heuristic, Task, Scheduler, CompletionData};
use rand::thread_rng;
use rand::seq::SliceRandom;


pub struct Random;

impl Heuristic for Random {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let mut ids: Vec<usize> = (0..tasks.len()).collect();
        ids.shuffle(&mut thread_rng());
        let mut scheduler = Scheduler::new();
        let mut curr_compl:u128 = 0;

        let mut task;
        for i in ids {
            task = tasks[i];
            curr_compl += task.period;

            let late = if curr_compl > task.due_to { curr_compl - task.due_to } else { 0 };
            let c = CompletionData::new(task.id, curr_compl, late);

            scheduler.tasks.insert(task, c);
        }

        scheduler
    }
}
