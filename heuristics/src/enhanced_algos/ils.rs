use crate::heuristic::BorrowedScheduler;
use crate::{enhanced_algos, Heuristic, Scheduler, Task};
use std::collections::BTreeSet;
use rand::distributions::{Distribution, Uniform};
use rand::thread_rng;

pub struct IlsAsIs5;
pub struct IlsRandom5;
pub struct IlsLessLate5;
pub struct IlsLessLateLessWeight5;
pub struct IlsMostLateMostWeight5;

pub struct IlsAsIs10;
pub struct IlsRandom10;
pub struct IlsLessLate10;
pub struct IlsLessLateLessWeight10;
pub struct IlsMostLateMostWeight10;

pub struct IlsAsIs20;
pub struct IlsRandom20;
pub struct IlsLessLate20;
pub struct IlsLessLateLessWeight20;
pub struct IlsMostLateMostWeight20;

fn compute_ils_style_hc_naive(scheduler:Scheduler, strength:usize) -> Scheduler {
    let mut rng = thread_rng();
    let size = scheduler.tasks.len();
    let nb_perturbations = size * strength / 100;
    let rand_pool = Uniform::from(0..size);

    let mut best = BorrowedScheduler::from(scheduler);
    let mut best_q = best.verify();

    for _iteration in 0..size/10 {
        // here we perform an ILS search with a maximum of n iterations...
        let tmp = best.clone();
        let mut items = tmp.get_refs();
        for _ in 0..nb_perturbations {
            // let's do some swaps
            let i = rand_pool.sample(&mut rng);
            let j = {
                let mut j = rand_pool.sample(&mut rng);
                while i == j {
                    j = rand_pool.sample(&mut rng);
                }
                j
            };
            let old_i = items[i];
            let old_j = items[j];
            items[j] = old_i;
            items[i] = old_j;
        }
        let sch = BorrowedScheduler::create_scheduler(&items);
        let upgraded = enhanced_algos::naive_climb::climbing_naively_unconverted(sch);

        let quality = upgraded.verify();
        if quality < best_q {
            best_q = quality;
            best = upgraded;
        }
    }
    let l = best.tasks.len();
    best.to_owned()
}

// 5

impl Heuristic for IlsAsIs5 {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler = crate::take_as_is::AsIs.solve_instance(tasks);
        compute_ils_style_hc_naive(scheduler, 5)
    }
}
impl Heuristic for IlsRandom5 {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler = crate::random::Random.solve_instance(tasks);
        compute_ils_style_hc_naive(scheduler, 5)
    }
}
impl Heuristic for IlsLessLate5 {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler = crate::constructive::ConstructiveHeuristicLessLate.solve_instance(tasks);
        compute_ils_style_hc_naive(scheduler, 5)
    }
}
impl Heuristic for IlsLessLateLessWeight5 {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler =
            crate::constructive::ConstructiveHeuristicLessLateLessWeight.solve_instance(tasks);
        compute_ils_style_hc_naive(scheduler, 5)
    }
}
impl Heuristic for IlsMostLateMostWeight5 {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler =
            crate::constructive::ConstructiveHeuristicMostLateMostWeight.solve_instance(tasks);
        compute_ils_style_hc_naive(scheduler, 5)
    }
}

impl Heuristic for IlsAsIs10 {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler = crate::take_as_is::AsIs.solve_instance(tasks);
        compute_ils_style_hc_naive(scheduler, 10)
    }
}

impl Heuristic for IlsRandom10 {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler = crate::random::Random.solve_instance(tasks);
        compute_ils_style_hc_naive(scheduler, 10)
    }
}

impl Heuristic for IlsLessLate10 {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler = crate::constructive::ConstructiveHeuristicLessLate.solve_instance(tasks);
        compute_ils_style_hc_naive(scheduler, 10)
    }
}
impl Heuristic for IlsLessLateLessWeight10 {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler =
            crate::constructive::ConstructiveHeuristicLessLateLessWeight.solve_instance(tasks);
        compute_ils_style_hc_naive(scheduler, 10)
    }
}
impl Heuristic for IlsMostLateMostWeight10 {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler =
            crate::constructive::ConstructiveHeuristicMostLateMostWeight.solve_instance(tasks);
        compute_ils_style_hc_naive(scheduler, 10)
    }
}

impl Heuristic for IlsAsIs20 {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler = crate::take_as_is::AsIs.solve_instance(tasks);
        compute_ils_style_hc_naive(scheduler, 20)
    }
}

impl Heuristic for IlsRandom20 {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler = crate::random::Random.solve_instance(tasks);
        compute_ils_style_hc_naive(scheduler, 20)
    }
}

impl Heuristic for IlsLessLate20 {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler = crate::constructive::ConstructiveHeuristicLessLate.solve_instance(tasks);
        compute_ils_style_hc_naive(scheduler, 20)
    }
}
impl Heuristic for IlsLessLateLessWeight20 {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler =
            crate::constructive::ConstructiveHeuristicLessLateLessWeight.solve_instance(tasks);
        compute_ils_style_hc_naive(scheduler, 20)
    }
}
impl Heuristic for IlsMostLateMostWeight20 {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler =
            crate::constructive::ConstructiveHeuristicMostLateMostWeight.solve_instance(tasks);
        compute_ils_style_hc_naive(scheduler, 20)
    }
}
