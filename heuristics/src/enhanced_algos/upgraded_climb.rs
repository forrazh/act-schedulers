use std::borrow::Borrow;
use std::cell::RefCell;
use crate::heuristic::BorrowedScheduler;
use crate::{Heuristic, Scheduler, Task};
use std::collections::BTreeSet;
use std::rc::Rc;
use std::sync::{Arc, Mutex};
use std::thread;

pub struct UpgradedClimbAsIs;
pub struct UpgradedClimbRandom;
pub struct UpgradedClimbLessLate;
pub struct UpgradedClimbLessLateLessWeight;
pub struct UpgradedClimbMostLateMostWeight;

use rayon::prelude::*;
use rayon::iter::ParallelIterator;

fn climbing_constant_upgrade(scheduler: Scheduler) -> Scheduler {
    let best_neighbour = BorrowedScheduler::from(scheduler);
    let best_quality = best_neighbour.verify();
    let cpy = best_neighbour.clone();
    let items = Box::new(cpy.get_refs());
    let no_items = items.len();
    let config_seen = Arc::new(Mutex::new({
        let mut s = BTreeSet::new();
        let neighbour = items.clone();
        s.insert(neighbour);
        s
    }));
    let config_to_test = Arc::new(Mutex::new({
        let mut v = Vec::new();
        v.push(items);
        v
    }));
    let mut best_quality = Arc::new(Mutex::new(best_quality));
    let mut best_neighbour = Arc::new(Mutex::new(best_neighbour));

    while let Some(items) = {
        let mut to_t = config_to_test.lock().unwrap();
        (*to_t).pop()
    } {
        // Use rayon's par_iter to parallelize the loop over `i`
        (0..no_items).into_par_iter().for_each(|i| {
            let mut config_to_test = Arc::clone(&config_to_test);
            let mut config_seen = Arc::clone(&config_seen);
            let mut items = items.clone();
            for j in (i + 1)..no_items {
                let old_i = items[i];
                let old_j = items[j];
                items[i] = old_j;
                items[j] = old_i;

                let contained = {
                    let seen = config_seen.lock().unwrap();
                    seen.contains(&items)
                };

                if !contained {
                    let ng = items.clone(); // 100
                    let sch = BorrowedScheduler::create_scheduler(&ng);
                    let q = sch.verify();
                    let mut best_quality = best_quality.lock().unwrap();
                    if q < *best_quality {
                        *best_quality = q;
                        let mut best_neighbour = best_neighbour.lock().unwrap();
                        *best_neighbour = sch;

                        let mut to_t = config_to_test.lock().unwrap();
                        (*to_t).push(ng.clone()); // 100
                    }
                    let mut seen = config_seen.lock().unwrap();
                    (*seen).insert(ng);
                }
                items[i] = old_i;
                items[j] = old_j;
            }
        });
    }

    let x = best_neighbour.lock().unwrap().clone().to_owned(); x
}

impl Heuristic for UpgradedClimbAsIs {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler = crate::take_as_is::AsIs.solve_instance(tasks);
        climbing_constant_upgrade(scheduler)
    }
}

impl Heuristic for UpgradedClimbRandom {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler = crate::random::Random.solve_instance(tasks);
        climbing_constant_upgrade(scheduler)
    }
}

impl Heuristic for UpgradedClimbLessLate {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler = crate::constructive::ConstructiveHeuristicLessLate.solve_instance(tasks);
        climbing_constant_upgrade(scheduler)
    }
}
impl Heuristic for UpgradedClimbLessLateLessWeight {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler =
            crate::constructive::ConstructiveHeuristicLessLateLessWeight.solve_instance(tasks);
        climbing_constant_upgrade(scheduler)
    }
}
impl Heuristic for UpgradedClimbMostLateMostWeight {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let scheduler =
            crate::constructive::ConstructiveHeuristicMostLateMostWeight.solve_instance(tasks);
        climbing_constant_upgrade(scheduler)
    }
}
