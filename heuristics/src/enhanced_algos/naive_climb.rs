use crate::heuristic::BorrowedScheduler;
use crate::{Heuristic, Scheduler, Task};
use std::collections::BTreeSet;
use std::sync::{Arc, Mutex};
use rayon::prelude::*;

pub struct NaiveClimbAsIs;

pub struct NaiveClimbRandom;

pub struct NaiveClimbLessLate;

pub struct NaiveClimbLessLateLessWeight;

pub struct NaiveClimbMostLateMostWeight;

pub fn climbing_naively_unconverted(best_neighbour: BorrowedScheduler) -> BorrowedScheduler {
    let best_quality = best_neighbour.verify();
    let cpy = best_neighbour.clone();
    let mut items = Box::new(cpy.get_refs());
    let mut best_quality = Arc::new(Mutex::new(best_quality));
    let mut best_neighbour = Arc::new(Mutex::new(best_neighbour));

    let no_items = items.len();
    (0..no_items).into_par_iter().for_each(|i| {
        let mut items = items.clone();
        for j in (i + 1)..no_items {
            let old_i = items[i];
            let old_j = items[j];
            items[i] = old_j;
            items[j] = old_i;

            let sch = BorrowedScheduler::create_scheduler(&items);
            let q = sch.verify();

            {
                let mut iter = sch.tasks.iter();
                let mut last = iter.next().unwrap();
                while let Some(curr) = iter.next() {
                    if last.1.completed_at > curr.1.completed_at {
                        panic!("Mais, je vais cabler...");
                    }
                    last = curr;
                }
            }

            let mut best_quality = best_quality.lock().unwrap();
            if q < *best_quality {
                *best_quality = q;
                let mut best_neighbour = best_neighbour.lock().unwrap();
                *best_neighbour = sch;
            }
            items[i] = old_i;
            items[j] = old_j;
        }
    });

    let x = best_neighbour.lock().unwrap().clone(); x
}

    fn climbing_naively(scheduler: Scheduler) -> Scheduler {
        let scheduler = BorrowedScheduler::from(scheduler);
        climbing_naively_unconverted(scheduler).to_owned()
    }

    impl Heuristic for NaiveClimbAsIs {
        fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
            let scheduler = crate::take_as_is::AsIs.solve_instance(tasks);
            climbing_naively(scheduler)
        }
    }

    impl Heuristic for NaiveClimbRandom {
        fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
            let scheduler = crate::random::Random.solve_instance(tasks);
            climbing_naively(scheduler)
        }
    }

    impl Heuristic for NaiveClimbLessLate {
        fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
            let scheduler = crate::constructive::ConstructiveHeuristicLessLate.solve_instance(tasks);
            climbing_naively(scheduler)
        }
    }

    impl Heuristic for NaiveClimbLessLateLessWeight {
        fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
            let scheduler =
                crate::constructive::ConstructiveHeuristicLessLateLessWeight.solve_instance(tasks);
            climbing_naively(scheduler)
        }
    }

    impl Heuristic for NaiveClimbMostLateMostWeight {
        fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
            let scheduler =
                crate::constructive::ConstructiveHeuristicMostLateMostWeight.solve_instance(tasks);
            climbing_naively(scheduler)
        }
    }
