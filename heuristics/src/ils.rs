use std::io::Write;
use crate::{Heuristic, Task};
use crate::heuristic::{BorrowedScheduler, Scheduler};
use rand::thread_rng;
use rand::distributions::{Distribution, Uniform};
use crate::verify::verify;

pub struct ILSRandomHCNaive;
pub struct ILSBadHeuristicHCNaive;
pub struct ILSMidHeuristicHCNaive;


fn compute_ils_style_hc_naive(scheduler:Scheduler, strength:usize) -> BorrowedScheduler {
    let mut rng = thread_rng();
    let size = scheduler.tasks.len();
    let nb_perturbations = size * strength / 100;
    let rand_pool = Uniform::from(0..size);

    let mut best = BorrowedScheduler::from(scheduler);
    let mut best_q = best.verify();

    for _iteration in 0..size {
        // here we perform an ILS search with a maximum of n iterations...
        let tmp = best.clone();
        let mut items = tmp.get_refs();
        for _ in 0..nb_perturbations {
            // let's do some swaps
            let i = rand_pool.sample(&mut rng);
            let j = {
                let mut j = rand_pool.sample(&mut rng);
                while i == j {
                    j = rand_pool.sample(&mut rng);
                }
                j
            };
            let old_i = items[i];
            let old_j = items[j];
            items[j] = old_i;
            items[i] = old_j;
        }
        let sch = BorrowedScheduler::create_scheduler(&items);
        let upgraded = crate::hill_climbing::climbing_naively_unconverted(sch);

        let quality = upgraded.verify();
        if quality < best_q {
            best_q = quality;
            best = upgraded;
        }
    }
    let l = best.tasks.len();
    best
}

impl Heuristic for ILSRandomHCNaive {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let sch = crate::random::Random.solve_instance(tasks);
        let sch = compute_ils_style_hc_naive(sch,20).to_owned();
        sch
    }
}

impl Heuristic for ILSBadHeuristicHCNaive {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let sch = crate::constructive::ConstructiveHeuristicLessLateLessWeight.solve_instance(tasks);
        let sch = compute_ils_style_hc_naive(sch, 5).to_owned();
        sch
    }
}


impl Heuristic for ILSMidHeuristicHCNaive {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let sch = crate::hill_climbing::HillClimbingNaiveML.solve_instance(tasks);
        let sch = compute_ils_style_hc_naive(sch, 5).to_owned();
        sch
    }
}

