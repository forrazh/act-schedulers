use std::collections::BTreeSet;
use std::fs::File;
use std::io;
use std::io::Read;
use std::path::Path;

use crate::heuristic::Scheduler;
use crate::verify::{show_scheduler, show_scheduler_in_file};
use heuristic::Heuristic;
use heuristic::Task;

mod constructive;
mod enhanced_algos;
mod heuristic;
mod random;
mod take_as_is;
mod verify;

fn read_data() -> Vec<Task> {
    // Read how many lines are coming
    let mut input_text = String::new();
    io::stdin()
        .read_line(&mut input_text)
        .expect("failed to read from stdin");
    let mut iter = input_text.lines();
    iter.next();
    let mut tasks: Vec<Task> = Vec::new();

    let mut i = 0;
    for it in iter {
        let t_i: Vec<u128> = input_text
            .split_whitespace()
            .map(|s| s.parse::<u128>().expect("failed to read an int"))
            .collect();
        tasks.push(Task::new(i, t_i[0], t_i[1], t_i[2]));
        i += 1;
    }

    let trimmed = input_text.trim();
    match trimmed.parse::<u32>() {
        Ok(line) => {
            for i in 0..line {
                input_text = String::from("");
                io::stdin()
                    .read_line(&mut input_text)
                    .expect("failed to read from stdin");
                //            println!("{input_text}");
                let t_i: Vec<u128> = input_text
                    .split_whitespace()
                    .map(|s| s.parse::<u128>().expect("failed to read an int"))
                    .collect();
                tasks.push(Task::new(i, t_i[0], t_i[1], t_i[2]));
            }
        }
        Err(..) => println!("this was not an integer: {}", trimmed),
    };

    tasks
}

fn read_from_file<P: AsRef<Path>>(path: P) -> io::Result<Vec<Task>> {
    let mut file_p = File::open(path)?;
    let mut input_text = String::new();
    file_p.read_to_string(&mut input_text)?;

    let mut iter = input_text.lines();
    iter.next();
    let mut tasks: Vec<Task> = Vec::new();

    let mut i = 0;
    for it in iter {
        let t_i: Vec<u128> = it
            .split_whitespace()
            .map(|s| s.parse::<u128>().expect("failed to read an int"))
            .collect();
        tasks.push(Task::new(i, t_i[0], t_i[1], t_i[2]));
        i += 1;
    }

    Ok(tasks)
}

const START_PATH: &str = "../SMTWP/";
const OUTPUT_PATH: &str = "./output/";
const FILE_NAMES: [&str; 20] = [
    "n100_15_b.txt",
    "n100_16_b.txt",
    "n100_17_b.txt",
    "n100_18_b.txt",
    "n100_19_b.txt",
    "n100_35_b.txt",
    "n100_36_b.txt",
    "n100_37_b.txt",
    "n100_38_b.txt",
    "n100_39_b.txt",
    "n100_40_b.txt",
    "n100_41_b.txt",
    "n100_42_b.txt",
    "n100_43_b.txt",
    "n100_44_b.txt",
    "n100_85_b.txt",
    "n100_86_b.txt",
    "n100_87_b.txt",
    "n100_88_b.txt",
    "n100_89_b.txt",
    // "n1000_1_b.txt",
    // "n1000_2_b.txt",
];

fn main() -> io::Result<()> {
    let heuristics: [(&str, Box<dyn Heuristic>); 33] = [
        // ("AsIs", Box::new(take_as_is::AsIs)),
        ("Random", Box::new(random::Random)),
        (
            "ConstructiveHeuristicMostLate",
            Box::new(constructive::ConstructiveHeuristicMostLate),
        ),
        (
            "ConstructiveHeuristicLessLate",
            Box::new(constructive::ConstructiveHeuristicLessLate),
        ),
        (
            "ConstructiveHeuristicLessWeight",
            Box::new(constructive::ConstructiveHeuristicLessWeight),
        ),
        (
            "ConstructiveHeuristicMostWeight",
            Box::new(constructive::ConstructiveHeuristicMostWeight),
        ),
        (
            "ConstructiveHeuristicLessLateLessWeight",
            Box::new(constructive::ConstructiveHeuristicLessLateLessWeight),
        ),
        (
            "ConstructiveHeuristicLessLateMostWeight",
            Box::new(constructive::ConstructiveHeuristicLessLateMostWeight),
        ),
        (
            "ConstructiveHeuristicMostLateLessWeight",
            Box::new(constructive::ConstructiveHeuristicMostLateLessWeight),
        ),
        (
            "ConstructiveHeuristicMostLateMostWeight",
            Box::new(constructive::ConstructiveHeuristicMostLateMostWeight),
        ),
        (
            "NaiveClimbRandom",
            Box::new(enhanced_algos::naive_climb::NaiveClimbRandom),
        ),
        (
            "NaiveClimbLessLate",
            Box::new(enhanced_algos::naive_climb::NaiveClimbLessLate),
        ),
        (
            "NaiveClimbLessLateLessWeight",
            Box::new(enhanced_algos::naive_climb::NaiveClimbLessLateLessWeight),
        ),
        (
            "NaiveClimbMostLateMostWeight",
            Box::new(enhanced_algos::naive_climb::NaiveClimbMostLateMostWeight),
        ),
        // //
        (
            "UpgradedClimbRandom",
            Box::new(enhanced_algos::upgraded_climb::UpgradedClimbRandom),
        ),
        (
            "UpgradedClimbLessLate",
            Box::new(enhanced_algos::upgraded_climb::UpgradedClimbLessLate),
        ),
        (
            "UpgradedClimbLessLateLessWeight",
            Box::new(enhanced_algos::upgraded_climb::UpgradedClimbLessLateLessWeight),
        ),
        (
            "UpgradedClimbMostLateMostWeight",
            Box::new(enhanced_algos::upgraded_climb::UpgradedClimbMostLateMostWeight),
        ),
        //
        ("VndRandom", Box::new(enhanced_algos::vnd::VndRandom)),
        ("VndLessLate", Box::new(enhanced_algos::vnd::VndLessLate)),
        (
            "VndLessLateLessWeight",
            Box::new(enhanced_algos::vnd::VndLessLateLessWeight),
        ),
        (
            "VndMostLateMostWeight",
            Box::new(enhanced_algos::vnd::VndMostLateMostWeight),
        ),
        // //
        ("IlsRandom5", Box::new(enhanced_algos::ils::IlsRandom5)),
        ("IlsLessLate5", Box::new(enhanced_algos::ils::IlsLessLate5)),
        (
            "IlsLessLateLessWeight5",
            Box::new(enhanced_algos::ils::IlsLessLateLessWeight5),
        ),
        (
            "IlsMostLateMostWeight5",
            Box::new(enhanced_algos::ils::IlsMostLateMostWeight5),
        ),
        // //
        ("IlsRandom10", Box::new(enhanced_algos::ils::IlsRandom10)),
        (
            "IlsLessLate10",
            Box::new(enhanced_algos::ils::IlsLessLate10),
        ),
        (
            "IlsLessLateLessWeight10",
            Box::new(enhanced_algos::ils::IlsLessLateLessWeight10),
        ),
        (
            "IlsMostLateMostWeight10",
            Box::new(enhanced_algos::ils::IlsMostLateMostWeight10),
        ),
        // //
        ("IlsRandom20", Box::new(enhanced_algos::ils::IlsRandom20)),
        (
            "IlsLessLate20",
            Box::new(enhanced_algos::ils::IlsLessLate20),
        ),
        (
            "IlsLessLateLessWeight20",
            Box::new(enhanced_algos::ils::IlsLessLateLessWeight20),
        ),
        (
            "IlsMostLateMostWeight20",
            Box::new(enhanced_algos::ils::IlsMostLateMostWeight20),
        ),
    ];

    let mut header = Vec::new();
    let mut results = Vec::new();

    for (n, _) in &heuristics {
        results.push((*n, Vec::new()));
    }
    results.push(("Best", Vec::new()));
    for f in FILE_NAMES {
        let file_name = START_PATH.to_owned() + f;
        // println!("-- Looking for <{file_name}> --");
        let vals = read_from_file(&file_name)?;
        println!("On {file_name}...");
        header.push(file_name);
        let mut cpt = 0;

        let mut best_q = u128::MAX;
        let mut best = Scheduler::new();
        for (_n, h) in &heuristics {
            let m_tasks: Vec<Task> = (&vals).to_owned(); //.into_iter().collect();
            let a_scheduler = h.solve_instance(m_tasks);
            let curr_quality = verify::verify(&a_scheduler);

            if curr_quality < best_q {
                best_q = curr_quality;
                best = a_scheduler;
            }

            results[cpt].1.push(curr_quality);
            cpt += 1;
            println!("	✔️  {_n}");
        }
        results[cpt].1.push(best_q);
        let output_name = OUTPUT_PATH.to_owned() + f;
        let mut output = File::create(output_name).unwrap();
        show_scheduler_in_file(output, &best);
    }

    let cols = header.len();
    show_line(("Heuristic / FILE_NAME", header));
    for i in 0..=cols {
        print!("| --- ");
    }
    println!("|");
    for r in results {
        show_line(r);
    }

    Ok(())
}

fn show_line<T: std::fmt::Display>((left, values): (&str, Vec<T>)) {
    print!("| {left}");
    for v in values {
        print!(" | {v}");
    }
    println!(" |");
}
