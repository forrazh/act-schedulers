use crate::heuristic::{Heuristic, Task, Scheduler, CompletionData};

pub struct AsIs;

impl Heuristic for AsIs {
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler {
        let mut scheduler = Scheduler::new();
        let mut curr_compl:u128 = 0;

        let mut task:Task;
        for i in 0..tasks.len() {
            task = tasks[i];
            curr_compl += task.period;

            let late = if curr_compl > task.due_to { curr_compl - task.due_to } else { 0 };
            let c = CompletionData::new(task.id, curr_compl, late);

            let _v = scheduler.tasks.insert(task, c);
        }
        scheduler
    }
}
