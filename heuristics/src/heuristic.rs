use std::collections::HashMap;

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy, Ord, PartialOrd)]
pub struct Task {
    pub id: u32,
    pub period: u128, // duration of a task
    pub weight: u128, // priority of a task
    pub due_to: u128, // if finished after, task is late
}

#[derive(Clone, Copy, Debug)]
pub struct CompletionData {
    _id: u32,
    pub completed_at: u128, // when was that task completed
    pub lateness: u128,     // How late is a given task
}

impl CompletionData {
    pub fn new(i: u32, c_at: u128, l: u128) -> CompletionData {
        CompletionData {
            _id: i,
            completed_at: c_at,
            lateness: l,
        }
    }
}

#[derive(Clone)]
pub struct Scheduler {
    pub tasks: HashMap<Task, CompletionData>, // both ids should be the same
}

impl Scheduler {
    pub(crate) fn new() -> Scheduler {
        Scheduler {
            tasks: HashMap::new(),
        }
    }
}

pub trait Heuristic {
    /// Take a list of tasks, return how late those are
    fn solve_instance(&self, tasks: Vec<Task>) -> Scheduler;
}

//impl<T: ?Sized + Heuristic> Heuristic for Box<T> {}

impl Task {
    pub(crate) fn new(i: u32, pi: u128, wi: u128, di: u128) -> Task {
        Task {
            id: i,
            period: pi,
            weight: wi,
            due_to: di,
        }
    }
}

// helper
#[derive(Clone)]
pub struct BorrowedScheduler {
    pub tasks: Vec<(Task, CompletionData)>,
}
impl BorrowedScheduler {
    pub fn new() -> BorrowedScheduler {
        BorrowedScheduler { tasks: Vec::new() }
    }

    pub fn get_refs(&self) -> Vec<&Task> {
        self.tasks.iter().map(|(t, _c)| t).collect()
    }

    pub fn from(scheduler: Scheduler) -> BorrowedScheduler {
        let mut sch = BorrowedScheduler::new();
        let mut tasks: Vec<_> = scheduler
            .tasks
            .iter()
            .collect();
        tasks.sort_by(|(_, c), (_, c2)| c.completed_at.cmp(&c2.completed_at));

        for (t, c) in scheduler.tasks {
            sch.tasks.push((t, c));
        }

        sch
    }

    pub fn create_scheduler(tasks: &Vec<&Task>) -> BorrowedScheduler {
        let mut scheduler = BorrowedScheduler::new();
        let mut curr_compl: u128 = 0;

        for t in tasks {
            curr_compl += t.period;

            let late = if curr_compl > t.due_to {
                curr_compl - t.due_to
            } else {
                0
            };
            let c = CompletionData::new(t.id, curr_compl, late);

            scheduler.tasks.push((*t.clone(), c));
        }

        scheduler
    }

    pub fn to_owned(self) -> Scheduler {
        let mut sch = Scheduler::new();
        for (t, c) in self.tasks {
            sch.tasks.insert(t, c);
        }
        sch
    }
    pub fn verify(&self) -> u128 {
        //    let task = (scheduler.tasks).keys().max_by_key(|t|{t.due_to}).unwrap();
        //    let (task, compl) = scheduler.tasks.iter().max_by_key(|(_t, c)|{c.completed_at}).unwrap();

        let lateness = self
            .tasks
            .iter()
            .map(|(t, c)| t.weight as u128 * c.lateness)
            .reduce(|a, b| a + b);

        //    if let Some(c) = scheduler.tasks.get(task) {
        lateness.unwrap()
    }
}
unsafe impl Send for Task {}
