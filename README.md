Avant tout : Je n'ai pas fait les tests pour les instances de grande taille, quand j'arrive
sur mon algorithme de VND, celui ci fait crasher le programme avec un SIGKILL.

> L'origine de ce probleme vient probablement d'une saturation de la RAM et donc l'OS vient
> libérer ce qui prend le plus de place/performances...(instances de taille 1000 sur tous 
> les threads accessibles...). Ma mise en place des algorithmes et des structures de données 
> est assez probablement grandement améliorable.


Ce README sera plus lisible directement sur Gitlab, 
[cliquez ici si vous êtes sur le VPN](https://gitlab-etu.fil.univ-lille.fr/hugo.forraz.etu/tp-act/-/tree/main/tp4)
ou [là si vous n'y avez pas accès.](https://gitlab.com/forrazh/act-schedulers).

# Heuristiques

TP fait en Rust;

Comment le lancer :

Il y a deux méthodes...  
La première consiste à installer la toolchain Rust (toutes les informations sont disponibles
[ici](https://rust-lang.github.io/rustup/concepts/toolchains.html)) puis de lancer depuis le
dossier `heuristics` la commande suivante :

```shell
cargo run
```

Qui va compiler et lancer le code sur toutes les instances.

La deuxième ne marche que sous Linux, j'ai laissé un exécutable [ici](todo://add_link) et de le lancer. 

Je vous propose malgré tout une section [DATA](#data) qui vous montre les résultats obtenus
pour chaque fichier avec chaque heuristique. Vous pouvez aussi retrouver la meilleure heuristique 
dans le dossier suivant [`heuristics/output`](./heuristics/output).

## Qualité

Le code permettant de vérifier la qualité d'une heuristique est le suivant :
```rust
pub fn verify(scheduler:&Scheduler) -> u128 {
    let lateness = scheduler.tasks
        .iter()
        .map(|(t,c)| { t.weight as u128 * c.lateness })
        .reduce(|a, b|{ a + b });

    lateness.unwrap()
}
```

On vient simplement faire un map pour que les retards soient multipliés par les retards et en faire la somme.
On peut noter que l'on utilise ici un `unsigned int` de 128 bits pour être sûrs que quelle que soit la taille 
des données, on puisse en stocker la taille vérifiée en mémoire.

## Random

Implémentation de l'heuristique aléatoire se trouve ici, code présent [ici](./heuristics/src/random.rs).

## Constructive

Vous pouvez retrouver les différentes implémentations des heuristiques constructives que j'ai faites
[ici](./heuristics/src/constructive.rs).

C'est la catégorie où j'ai réalisé le plus d'heuristiques différentes étant donné que ce sont les moins
coûteuses en terme de temps de calcul donc beaucoup plus rapide à vérifier pour se faire une première 
idée.

Les 8 heuristiques proposées sont :
```rust
pub struct ConstructiveHeuristicMostWeight; // prioriser sur le poids seulement (les plus urgentes d'abord)
pub struct ConstructiveHeuristicLessWeight; // prioriser sur le poids seulement (les moins urgentes d'abord)
pub struct ConstructiveHeuristicLessLate; // prioriser sur le retard seulement (les moins en retard d'abord)
pub struct ConstructiveHeuristicMostLate; // prioriser sur le retard seulement (les plus en retard d'abord)

pub struct ConstructiveHeuristicMostLateMostWeight; 
pub struct ConstructiveHeuristicMostLateLessWeight;
pub struct ConstructiveHeuristicLessLateMostWeight;
pub struct ConstructiveHeuristicLessLateLessWeight;
```

Les 4 premières ne prennent en compte qu'un paramètre à la fois, poids ou retard et plus ou moins, là 
où les 4 suivantes prennent en compte les deux précédentes. Dans le cas des heuristiques combinées, on
vient d'abord faire une séparation similaire aux heuristiques constructives sur le poids puis l'on 
applique l'heuristique sur le retard.

Lorsque l'on analyse les résultats, on peut directement remarquer une chose (qui est assez logique), 
les heuristiques privilégiant les tâches les moins urgentes sont assez mauvaises.

La deuxième chose que l'on peut remarquer avec ces heuristiques est que les heuristiques privilégiant le 
poids sont souvent bonnes (dans une certaine mesure, elles sont loin d'être parfaites).

Pour la suite, on va continuer d'utiliser les heuristiques suivantes : 
- Random 
- ConstructiveHeuristicLessLate (avec des résultats qui semblent plutot dans la moyenne)
- ConstructiveHeuristicLessLateLessWeight (assez mauvaise)
- ConstructiveHeuristicMostLateMostWeight (en général la meilleure)

## HillClimbing

Vous pouvez retrouver les différentes implémentations de HillClimbing [ici](./heuristics/src/hill_climbing.rs).

Malheureusement, mes heuristiques de HillClimbing (et celles d'ILS proposées plus tard)
rencontrent un problème que je n'ai pas su résoudre : 
> Impossible de les mettre en place sans cloner les données des tableaux afin de réaliser 
> les swaps et de vérifier les qualités (de façon propre/lisible et sans réassigner à 
> chaque fois.

Vous pouvez retrouver [ici (enhanced_algos)](./heuristics/src/enhanced_algos) des
tentatives d'améliorations du point de vue performances (réassignations, multithreading)

La première assez naïve est la suivante : 

```rust
pub fn climbing_naively_unconverted(mut best_neighbour:BorrowedScheduler) -> BorrowedScheduler {
    let mut best_quality = best_neighbour.verify();
    let cpy = best_neighbour.clone();
    let items = Box::new(cpy.get_refs());

    let mut config_seen:BTreeSet<Box<Vec<&Task>>> = BTreeSet::new();
    config_seen.insert(items.clone());

    let no_items = items.len();
    for i in 0..no_items {
        for j in (i+1)..no_items {
            let mut nghb = items.clone(); 
            nghb[i] = items[j];
            nghb[j] = items[i];

            if !config_seen.contains(&nghb) {
                let ng = nghb.clone(); 
                let sch = BorrowedScheduler::create_scheduler(&ng);
                let q = sch.verify();
                if q < best_quality {
                    best_quality = q;
                    best_neighbour = sch;
                }
                config_seen.insert(nghb);
            }
        }
    }

    best_neighbour
}
```

On vient simplement pour une solution donnée vérifier toutes les permutations possibles 
ce qui donne $`n * (n-1)/2`$ permutations possibles à vérifier. Afin de simplifier le 
travail au processeur, on vient mémoïser les configurations qui ont déjà pu être rencontrées
(même si ce n'est pas pas forcément utile dans le cas d'un `HillClimbing` aussi simple).

La deuxième est celle ci :

```rust
fn climbing_constant_upgrade(scheduler:Scheduler) -> Scheduler{
    let mut best_neighbour = BorrowedScheduler::from(scheduler);
    let mut best_quality = best_neighbour.verify();
    let cpy = best_neighbour.clone();
    let items = Box::new(cpy.get_refs());
    let no_items = items.len();
    let mut config_seen:BTreeSet<Box<Vec<&Task>>> = BTreeSet::new();
    let mut config_to_test:Vec<Box<Vec<&Task>>> = Vec::new();

    let neighbour = items.clone();
    config_to_test.push(items);
    config_seen.insert(neighbour);

    while let Some(items) = config_to_test.pop() {
        for i in 0..no_items {
            for j in (i+1)..no_items {
                let mut nghb = items.clone(); // 100
                nghb[i] = items[j];
                nghb[j] = items[i];

                if !config_seen.contains(&nghb) {
                    let ng = nghb.clone(); // 100
                    let sch = BorrowedScheduler::create_scheduler(&ng);
                    let q = sch.verify();
                    if q < best_quality {
                        best_quality = q;
                        best_neighbour = sch;
                        config_to_test.push(nghb.clone()); // 100
                    }
                    config_seen.insert(nghb);
                }
            }
        }
    }

    best_neighbour.clone().to_owned()
}
```

On peut noter la chose suivante qui est assez importante par rapport à la première :
> On vient essayer de faire plusieurs permutations en gardant chaque 
> permutation améliorante (utilité de la mémoïsation).

---

> L'implémentation optimisée avec le multithreading (entre autre) est disponible 
> [ici pour la version naive](./heuristics/src/enhanced_algos/naive_climb.rs) et 
> [là pour la version "améliorée"](./heuristics/src/enhanced_algos/upgraded_climb.rs).


## VND

Après mes implémentations de `HillClimbing`, j'ai aussi réalisé l'implémentation de VND suivante : 

```rust
fn compute_vnd_style(scheduler:Scheduler) -> Scheduler{
    let items:Vec<&Task> = scheduler.tasks
        .iter()
        .map(|(t, _)|{t})
        .collect();

    let fst = BorrowedScheduler::create_scheduler(&items);
    let mut best_neighbour = fst.0;
    let items = fst.1;
    let mut best_quality = best_neighbour.verify();


    let max_items:usize = items.len();
    let max_iter:usize = max_items / 10;

    let mut config_seen:BTreeSet<Vec<&Task>> = BTreeSet::new();
    let mut config_to_test:Vec<Vec<&Task>> = Vec::new();
    config_seen.insert(items.clone());
    config_to_test.push(items);

    while let Some(items) = config_to_test.pop() {
        for iter in 0..max_iter {
            // n * (n-1)/2 iter
            for i in 0..max_items {
                for j in (i+1)..(max_items-iter) {
                    let mut other = items.clone();
                    other[i] = items[j+iter];
                    other[j+iter] = items[i];
                    if !config_seen.contains(&other) {
                        let (sch, o) = BorrowedScheduler::create_scheduler(Box::new(other));
                        let q = sch.verify();
                        if q < best_quality {
                            best_quality = q;
                            best_neighbour = sch;
                            config_to_test.push(other);
                        }
                        config_seen.insert(o);
                    }
                }
            }
        }
    }

    best_neighbour.to_owned()
}
```

> L'implémentation optimisée avec le multithreading (entre autre) est disponible [ici](./heuristics/src/enhanced_algos/vnd.rs).

Elle vient effectuer ce que l'on faisait avec la fonction `climbing_constant_upgrade` mais en 
allant jusqu'une distance pour les échanges de `n / 10`.

## ILS

Vous pouvez retrouver une implémentation de `ILS` [juste ici](./heuristics/src/ils.rs).

On vient ici avec un ordonnancement donné faire une portion d'échanges aléatoires (déjà dans l'algo,
j'ai réalisé des tests avec 5, 10 et 20% d'échanges afin d'avoir des résultats les plus différents 
possibles selon les heuristiques).

> L'implémentation optimisée avec le multithreading (entre autre) est disponible [ici](./heuristics/src/enhanced_algos/ils.rs).


## Data

Voici les différents scores obtenus pour chaque fichier avec chaque heuristique. Pour simplifier
la lecture, chaque colonne correspond à un fichier à ordonnancer et chaque ligne à une heuristique 
donnée.

| Heuristic / FILE_NAME                     | ../SMTWP/n100_15_b.txt   | ../SMTWP/n100_16_b.txt   | ../SMTWP/n100_17_b.txt   | ../SMTWP/n100_18_b.txt   | ../SMTWP/n100_19_b.txt   | ../SMTWP/n100_35_b.txt   | ../SMTWP/n100_36_b.txt   | ../SMTWP/n100_37_b.txt   | ../SMTWP/n100_38_b.txt   | ../SMTWP/n100_39_b.txt   | ../SMTWP/n100_40_b.txt   | ../SMTWP/n100_41_b.txt   | ../SMTWP/n100_42_b.txt   | ../SMTWP/n100_43_b.txt   | ../SMTWP/n100_44_b.txt   | ../SMTWP/n100_85_b.txt   | ../SMTWP/n100_86_b.txt   | ../SMTWP/n100_87_b.txt   | ../SMTWP/n100_88_b.txt   | ../SMTWP/n100_89_b.txt   |
|-------------------------------------------|--------------------------|--------------------------|--------------------------|--------------------------|--------------------------|--------------------------|--------------------------|--------------------------|--------------------------|--------------------------|--------------------------|--------------------------|--------------------------|--------------------------|--------------------------|--------------------------|--------------------------|--------------------------|--------------------------|--------------------------|
| Random                                    | 490127                   | 865690                   | 901845                   | 972155                   | 997624                   | 172941                   | 403427                   | 590543                   | 517298                   | 498952                   | 498671                   | 1147103                  | 1010238                  | 896859                   | 1002632                  | 303770                   | 589880                   | 597172                   | 612343                   | 483861                   |
| ConstructiveHeuristicMostLate             | 478227                   | 981102                   | 936027                   | 1005176                  | 999227                   | 314240                   | 611081                   | 710258                   | 686042                   | 600524                   | 639564                   | 1323436                  | 1058089                  | 957094                   | 989860                   | 491609                   | 831659                   | 785170                   | 788932                   | 668214                   |
| ConstructiveHeuristicLessLate             | 507535                   | 873886                   | 815071                   | 1051000                  | 990182                   | 80490                    | 316301                   | 496070                   | 340580                   | 428149                   | 441721                   | 911922                   | 908570                   | 855628                   | 821220                   | 1007                     | 273881                   | 357374                   | 228148                   | 213710                   |
| ConstructiveHeuristicLessWeight           | 673843                   | 1207226                  | 1198148                  | 1240148                  | 1228144                  | 316968                   | 696986                   | 816219                   | 814742                   | 710611                   | 690071                   | 1571293                  | 1273328                  | 1347454                  | 1213573                  | 490111                   | 899458                   | 860488                   | 809160                   | 703161                   |
| ConstructiveHeuristicMostWeight           | 304872                   | 609941                   | 525957                   | 814649                   | 730092                   | 92327                    | 253447                   | 372026                   | 220369                   | 351395                   | 325482                   | 658604                   | 650132                   | 481348                   | 603218                   | 156324                   | 282417                   | 357604                   | 312904                   | 303116                   |
| ConstructiveHeuristicLessLateLessWeight   | 711502                   | 1239252                  | 1241165                  | 1287140                  | 1275466                  | 350457                   | 736226                   | 856112                   | 859555                   | 753003                   | 729532                   | 1610928                  | 1323163                  | 1386798                  | 1269462                  | 533638                   | 943947                   | 904483                   | 864363                   | 749426                   |
| ConstructiveHeuristicLessLateMostWeight   | 288437                   | 591017                   | 499859                   | 779062                   | 690113                   | 89215                    | 242088                   | 364346                   | 216723                   | 326173                   | 318906                   | 642770                   | 625353                   | 467466                   | 586396                   | 160472                   | 288944                   | 365267                   | 314577                   | 299317                   |
| ConstructiveHeuristicMostLateLessWeight   | 712177                   | 1244500                  | 1236142                  | 1287797                  | 1274959                  | 338358                   | 716602                   | 852809                   | 847554                   | 742641                   | 733576                   | 1607838                  | 1322736                  | 1372416                  | 1254392                  | 489329                   | 921014                   | 887493                   | 821556                   | 722059                   |
| ConstructiveHeuristicMostLateMostWeight   | 277459                   | 578933                   | 484182                   | 774930                   | 698814                   | 76972                    | 221634                   | 347194                   | 184656                   | 317951                   | 295289                   | 614060                   | 604755                   | 456561                   | 556856                   | 128713                   | 250053                   | 323576                   | 259938                   | 259634                   |
| NaiveClimbRandom                          | 407450                   | 881948                   | 742991                   | 925390                   | 893802                   | 169076                   | 412062                   | 578051                   | 556571                   | 478368                   | 431902                   | 1101659                  | 912760                   | 827414                   | 832770                   | 252061                   | 540938                   | 592401                   | 471585                   | 477648                   |
| NaiveClimbLessLate                        | 389131                   | 829510                   | 801981                   | 969130                   | 920182                   | 80490                    | 316301                   | 493522                   | 340580                   | 428149                   | 415347                   | 911922                   | 860554                   | 843645                   | 821220                   | 1007                     | 273881                   | 357374                   | 228148                   | 213710                   |
| NaiveClimbLessLateLessWeight              | 502013                   | 868859                   | 831883                   | 976305                   | 1013366                  | 149792                   | 372419                   | 636837                   | 398146                   | 475617                   | 421635                   | 1117000                  | 980964                   | 852461                   | 803158                   | 267544                   | 465551                   | 580753                   | 457236                   | 511663                   |
| NaiveClimbMostLateMostWeight              | 277459                   | 578933                   | 484182                   | 774930                   | 698814                   | 76972                    | 221634                   | 347194                   | 184656                   | 317951                   | 295289                   | 614060                   | 604755                   | 456561                   | 556856                   | 128713                   | 250053                   | 323576                   | 259938                   | 259634                   |
| UpgradedClimbRandom                       | 173202                   | 408317                   | 810028                   | 546987                   | 478028                   | 200231                   | 470907                   | 183236                   | 92629                    | 498482                   | 131398                   | 1011711                  | 426542                   | 844750                   | 362630                   | 519                      | 69619                    | 85936                    | 57998                    | 57613                    |
| UpgradedClimbLessLate                     | 174142                   | 407908                   | 815071                   | 546231                   | 479163                   | 80490                    | 316301                   | 496070                   | 340580                   | 428149                   | 441721                   | 911922                   | 908570                   | 855628                   | 361307                   | 1007                     | 273881                   | 357374                   | 228148                   | 213710                   |
| UpgradedClimbLessLateLessWeight           | 174983                   | 408219                   | 333964                   | 545196                   | 478634                   | 20579                    | 109638                   | 182647                   | 93033                    | 155200                   | 133194                   | 462483                   | 426047                   | 320693                   | 361179                   | 688                      | 71587                    | 86141                    | 58138                    | 57516                    |
| UpgradedClimbMostLateMostWeight           | 277459                   | 578933                   | 484182                   | 774930                   | 698814                   | 76972                    | 221634                   | 347194                   | 184656                   | 317951                   | 295289                   | 614060                   | 604755                   | 456561                   | 556856                   | 128713                   | 250053                   | 323576                   | 259938                   | 259634                   |
| VndRandom                                 | 174326                   | 408342                   | 337657                   | 544968                   | 478841                   | 20041                    | 110787                   | 184938                   | 94225                    | 156282                   | 131744                   | 462679                   | 428631                   | 321602                   | 361059                   | 507                      | 70302                    | 85873                    | 58906                    | 533537                   |
| VndLessLate                               | 175180                   | 873886                   | 334769                   | 547351                   | 990182                   | 80490                    | 316301                   | 496070                   | 340580                   | 428149                   | 441721                   | 911922                   | 426467                   | 855628                   | 821220                   | 1007                     | 273881                   | 357374                   | 228148                   | 213710                   |
| VndLessLateLessWeight                     | 174475                   | 408150                   | 335755                   | 546722                   | 478299                   | 20374                    | 110116                   | 183918                   | 93307                    | 154718                   | 130183                   | 463251                   | 426522                   | 320820                   | 362612                   | 544                      | 76126                    | 86776                    | 60779                    | 57678                    |
| VndMostLateMostWeight                     | 277459                   | 578933                   | 484182                   | 774930                   | 698814                   | 76972                    | 221634                   | 347194                   | 184656                   | 317951                   | 295289                   | 614060                   | 604755                   | 456561                   | 556856                   | 128713                   | 250053                   | 323576                   | 259938                   | 259634                   |
| IlsRandom5                                | 316055                   | 645490                   | 563370                   | 756781                   | 736920                   | 80388                    | 277164                   | 430105                   | 348360                   | 378611                   | 335895                   | 1088060                  | 710615                   | 639107                   | 622578                   | 178615                   | 358543                   | 566451                   | 310580                   | 293919                   |
| IlsLessLate5                              | 343397                   | 694411                   | 663741                   | 756699                   | 720409                   | 80490                    | 316301                   | 496070                   | 340580                   | 428149                   | 441721                   | 911922                   | 697732                   | 662509                   | 821220                   | 1007                     | 273881                   | 357374                   | 228148                   | 213710                   |
| IlsLessLateLessWeight5                    | 293933                   | 749494                   | 583657                   | 768587                   | 803981                   | 91456                    | 275099                   | 454044                   | 323758                   | 357189                   | 348061                   | 850765                   | 751092                   | 618574                   | 642366                   | 157399                   | 332343                   | 364805                   | 310407                   | 352235                   |
| IlsMostLateMostWeight5                    | 277459                   | 578933                   | 484182                   | 774930                   | 698814                   | 76972                    | 221634                   | 347194                   | 184656                   | 317951                   | 295289                   | 614060                   | 604755                   | 456561                   | 556856                   | 128713                   | 250053                   | 323576                   | 259938                   | 259634                   |
| IlsRandom10                               | 358403                   | 690989                   | 813792                   | 761284                   | 860727                   | 85839                    | 293236                   | 465798                   | 298292                   | 335282                   | 306970                   | 842382                   | 666223                   | 638577                   | 717788                   | 243918                   | 391850                   | 573150                   | 350131                   | 329058                   |
| IlsLessLate10                             | 335211                   | 873886                   | 640567                   | 813172                   | 771571                   | 80490                    | 316301                   | 496070                   | 340580                   | 428149                   | 441721                   | 911922                   | 908570                   | 647155                   | 821220                   | 1007                     | 273881                   | 357374                   | 228148                   | 213710                   |
| IlsLessLateLessWeight10                   | 320033                   | 692326                   | 620236                   | 833197                   | 749252                   | 94339                    | 332571                   | 409415                   | 364789                   | 350520                   | 315063                   | 852231                   | 755409                   | 771758                   | 708891                   | 170281                   | 374650                   | 400814                   | 310311                   | 290964                   |
| IlsMostLateMostWeight10                   | 277459                   | 578933                   | 484182                   | 774930                   | 698814                   | 76972                    | 221634                   | 347194                   | 184656                   | 317951                   | 295289                   | 614060                   | 604755                   | 456561                   | 556856                   | 128713                   | 250053                   | 323576                   | 259938                   | 259634                   |
| IlsRandom20                               | 387874                   | 725243                   | 603816                   | 829789                   | 772209                   | 150370                   | 336640                   | 390167                   | 374650                   | 355061                   | 340524                   | 820372                   | 747567                   | 613195                   | 678946                   | 148268                   | 425321                   | 417890                   | 392700                   | 325302                   |
| IlsLessLate20                             | 395440                   | 750891                   | 679143                   | 881548                   | 808822                   | 80490                    | 316301                   | 496070                   | 340580                   | 370304                   | 441721                   | 911922                   | 813102                   | 681747                   | 702814                   | 1007                     | 273881                   | 357374                   | 228148                   | 213710                   |
| IlsLessLateLessWeight20                   | 364859                   | 707362                   | 743118                   | 859441                   | 752702                   | 94798                    | 323961                   | 403378                   | 342475                   | 380168                   | 364801                   | 876217                   | 808193                   | 701205                   | 742844                   | 216451                   | 431226                   | 469781                   | 442954                   | 366302                   |
| IlsMostLateMostWeight20                   | 277459                   | 578933                   | 484182                   | 774930                   | 698814                   | 76972                    | 221634                   | 347194                   | 184656                   | 317951                   | 295289                   | 614060                   | 604755                   | 456561                   | 556856                   | 128713                   | 250053                   | 323576                   | 259938                   | 259634                   |
| ----------------------------------------- | ------------------------ | ------------------------ | ------------------------ | ------------------------ | ------------------------ | ------------------------ | ------------------------ | ------------------------ | ------------------------ | ------------------------ | ------------------------ | ------------------------ | ------------------------ | ------------------------ | ------------------------ | ------------------------ | ------------------------ | ------------------------ | ------------------------ | ------------------------ |
| Best                                      | 173202                   | 407908                   | 333964                   | 544968                   | 478028                   | 20041                    | 109638                   | 182647                   | 92629                    | 154718                   | 130183                   | 462483                   | 426047                   | 320693                   | 361059                   | 507                      | 69619                    | 85873                    | 57998                    | 57516                    |
